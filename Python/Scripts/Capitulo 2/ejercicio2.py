#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 23 15:46:50 2020

@author: alonso2499
"""

# Ejercicio 1, capítulo 2

print(1 + 3 * 2 - 5 + 4)

print(1 + 3 * (2 - 5) + 4)

print(5**2**2*3+1)

print(5**(2**2)*3+1)

print(1 + 3/2)

print(1+3.0/2)

print(-2-1)

print(-(2-1))

print(3.0 + 3/2)

print(3 + 3/2.0)

print(-1**0.5)
