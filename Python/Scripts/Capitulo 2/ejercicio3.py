#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 23 15:47:23 2020

@author: alonso2499
"""

# Ejericio 3, Capítulo 2

print(int('01001',2))

print(int(str(100),2))

print(int(str(1100),2))

print(int(str(11111),2))

print(int(str(1111111),2))
