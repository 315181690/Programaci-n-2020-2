#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 24 13:43:07 2020

@author: alonso2499
"""
cadena =input("Introduzca aquí la cadena\n")
def contarVocales(cadena):
    count=0
    for i in cadena:
        if i in "aeiouAEIOUaéíóúÜ":
            count+=1
    return (count)
print("El número de vocales es", contarVocales(cadena))

def contarConsonantes(cadena):
    count=0
    for i in cadena:
        if i not in " aeiouAEIOUaéíóúÜ":
            count+=1
    return(count)
print("EL número de consonantes es", contarConsonantes(cadena))

def contarConsonantes2(cadena):
    count=0
    cadenatmp=cadena.replace(" ","")
    for i in cadenatmp:
        if i not in "aeiouAEIOUaéíóúÜ":
            count+=1
    return(count)
print("EL número de consonantes es", contarConsonantes(cadena))

def palindromo(cadena):
    cadenatmp=cadena.replace(" ","")
    i=0
    j=len(cadenatmp)-1
    while(i<=j):
        if cadenatmp[i]!=cadenatmp[j]:
            return False
        i+=1
        j+=1
    return True
print(palindromo(cadena))
