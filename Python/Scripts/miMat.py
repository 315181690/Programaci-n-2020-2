# -*- coding: utf-8 -*-
"""
Editor de Spyder

Este es un archivo temporal.
"""

def vabs(a,e = 0.001):
    b = 1
    h =a 
    while abs(b-h)>e:
        h=a
        b = (b+h)/2;
        h = a/b
    return b 

def abs1(x):
    """
    primera version de valor absolutp
    """
    if x==0:
        return x
    elif x<0:
        x = -1*x
        return x
    elif x>0:
        return x



def miRaiz(a,e=0.00001):
    """
    Regresa una aproximación a la raíz cuadrada del número a.
    Se calcula construyendo rectángulos de área a cuyos lados 
    sean cada vez más parecidos.
    Se considera una aproximación aceptable cuando la diferencia
    de las longitudes de los lados es menor al error de 0.001
    """
    b=1
    h=a
    while abs(b-h)>=e:
        b=(b+h)/2;
        h=a/b
        return b
def mcd(a,b):
    q=a//b
    r=a-b*q
    while r !=0:
        a=b;
        b=r
        q=a//b
        r=a-b*q
    return b

def MCD(a,b,*c):
    resultado=mcd(a,b)
    for valor in c:
        resultado=mcd(resultado,valor)
    return resultado

if __name__ == "__main__":
    print("mcd(24,36,40)={}".format(MCD(24,36,40,30,35,37,39,1999)))
    print("mcd(24,36,40)={}".format(MCD(24,36)))
    print(mcd(85,25))
    print(vabs(-1024))