#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 12 13:06:22 2020

@author: alonso2499
"""
def cuadrados():
    ln=[]
    for i in range(10):
        l=str(input("Ingrese un número\n"))
        if l.isnumeric()==True:
            l=int(l)
            l*=l
            ln.append(l)
        else:
            return("El caracter ingresado no es un número, intente de nuevo")
    return ln

def numeros_perfectos(n):
    suma=0
    for i in range(1,n):
        if n%(i)==0:
            suma+=(i)
    if n == suma:
        return ("Es un número perfecto")
    else:
        return ("No es un número perfecto")