#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 18 14:18:48 2020

@author: alonso2499
"""
def Pascal(n):
    l1=[[1],[1,1]] #Iniciamos el triángulo con las primeras dos líneas
    for i in range(1,n):#for que se ejecutará por cada línea
        ln=[1]#Cada línea incia con un uno
        for j in range(0,len(l1[i])-1):#Tomará cada valor de cada línea
            ln.extend([l1[i][j]+l1[i][j+1]])#Suma del valor de la lista anterior con el que sigue
        ln+=[1]#Cada línea termina con un uno
        l1.append(ln)#Agregamos la linea a la lista con la líneas del triángulo
    for linea in l1:
        print(linea)#Muestra cada línea
print(Pascal(10))
