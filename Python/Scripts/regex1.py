#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  4 13:12:35 2020

@author: alonso2499
"""
import re
#bbbaaa (3,5) índices del primer patrón encontrado - re.match
#re.search - True o false si se cumple el patrón
#re.findall - lista
#? una aparición o ninguna

#Escribir un programa en python que tenga una a seguido de 0 o más b's
def empatatexto(text):
    patron_a_buscar = 'ab*'
    if re.search(patron_a_buscar, text):
        return ("Se encontró una coincidencia")
    else:
        return "No se encontraron coincidencias"
print(empatatexto("ac"))
print(empatatexto("abc"))
print(empatatexto("abbc"))

#Escribir un programa en python que tenga una a seguida de 1 o más b's

def empatatexto1(text):
    patron_a_buscar = 'ab+'
    if re.search(patron_a_buscar, text):
        return ("Se encontró una coincidencia")
    else:
        return "No se encontraron coincidencias"
print(empatatexto1("ac"))
print(empatatexto1("abc"))
print(empatatexto1("abbc"))

#Escribe un programa en Python que encuentre una cadena que tiene una
#a seguida de 3 b's

def empatatexto2(text):
    patron_a_buscar = 'ab{3}'
    if re.search(patron_a_buscar, text):
        return ("Se encontró una coincidencia")
    else:
        return "No se encontraron coincidencias"
print(empatatexto2("ac"))
print(empatatexto2("abc"))
print(empatatexto2("aabbbbc"))
print(empatatexto2("aabbb"))

#Escribe un programa en python para verificar que una cadena contenga
#solo un cierto conjunto de caracteres de a-z, A-Z y 0-9

def capermitido(string):
    string = re.search('^[a-zA-Z0-9]', string)
    return bool(string)
print(capermitido("ABCDEFábcdef123450"))
print(capermitido("#$%&[]}{"))

#Escribe un programa en python para encontrar secuencias de letras minúsculas
#unidas por un guión bajo ejemplo aa_cbb

def secuencias(string):
    patron= "[a-ñ]_+[a-ñ]$"
    if re.search(patron, string):
        return "Se encontraron coincidencias"
    else:
        return "No se encontraron coincidencias"
print(secuencias("a_b_c_z"))
print(secuencias("1_2_3_c"))
    