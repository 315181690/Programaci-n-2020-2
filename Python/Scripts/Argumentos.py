#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 17 13:51:38 2020

@author: alonso2499
"""

def printinfo(edad, *argumentos):
    "This prints a variable passed arguments"
    print("La edad es: ")
    print(edad)
    for argumento in argumentos:
        print(argumento)
    return
#Now you can call printinfo function
printinfo(10)
printinfo(70,60,50)