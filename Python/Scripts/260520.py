#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 26 13:10:43 2020

@author: alonso2499
"""

"Listas por comprensión"
#Sintaxis [[valor salida] for i in objeto iterable if [condiciones]

#Elevar al cuadrado los elementos de una lista
ls1=[5,10,15,20,25]
res=[]
for i in ls1:
    res.append(i**2)
print(res)

#Comprensión de listas elevar al cuadrado los elementos de una lista
res=[i**2 for i in ls1]
print(res)

#Elevar solo los números pares al cuadrado
ls2=[1,2,3,4,5,6,7]
res=[]
for i in ls2:
    if i%2==0:
        res.append(i**2)
print(res)

res=[i**2 for i in ls2 if i%2==0]
print(res)

res=[]
for i in ls2:
    if i%2==0:
        res.append(i**2)
    else:
        res.append(i**3)
print(res)

res=[i**2 if i%2==0 else i**3 for i in ls2]
print(res)

ls1=[9,3,6,1,5,0,8,2,4,7]
ls2=[6,4,6,1,2,2]
res=[]

for i in ls2:
    res.append((i, ls1.index(i)))
print(res)

res=[(i,ls1.index(i)) for i in ls2]
print(res)

res={}
for i in ls2:
    res.update({i:ls1.index(i)})
    
print(res)

res=res.update({i:ls1.index(i)} for i in ls2)
print(res)

