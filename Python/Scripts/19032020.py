#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 13:15:22 2020

@author: alonso2499
"""

""" for variable in elemento iterable(lista, cadena, range, etc)
cuerpo de ciclo for"""
"""primer programa utilizando for"""
"""
print("Comenzamos")
for i in [0,1,2]:
    print("Hola", end="")
print()
print("Final")

list=[1,3,5,7,9]
for i in list:
    print(i)
"""
"""Escribir un programa que pida al usuaerio una palabra y la muestre 15 veces en la pantalla"""
"""
palabra=input("Escriba aquí su palabra\n")
for i in range(15):
    print(palabra)
   """ 
"""Escribir un programa que pregunte al usuario su edad y muestre por pantalla todos los
añosn que ha cumplido desde que tenía 1"""
"""
edad=int(input("Escriba su edad\n"))
for i in range(edad):
    print("Hasta hoy has cumplido " + str(i+1) + " años")
    """
"""Escribir un programa que almacene las asignaturas de un curso y que en la pantalla
se muestren (Programación, Cálculo, Algebra)"""
l1=["Programación", "Cálculo", "Álgebra"]
"""for i in l1:
    print(i)"""
print(l1)