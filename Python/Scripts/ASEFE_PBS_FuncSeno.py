#!/usr/bin/env python3
#-*- coding: utf-8 -*-

from math import sin, sqrt

def seno(alfa):
    beta=alfa/(2**5)
    b=beta
    for i in range(5):
        b=2*sin(b)*sqrt(1-(sin(b)**2))
    print(b)

li=(6,5,4,3,2)
betas=[]
def seno1(alfa):
    for h in li:
        beta=alfa/2**(h-1)
        dosbeta=2*beta*sqrt(1-(beta**2))
        betas.append(beta)
        beta=dosbeta
    return(beta)
    