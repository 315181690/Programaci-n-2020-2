#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  2 13:12:57 2020

@author: alonso2499
"""
import re
"""Regex o expresión regular, es una secuencia de caracteres que forma un patrón
de búsqueda"""

"[] cada que quieras definir un conjunto de caracteres 'permitidos'  o un"
"patrón vamos a escribirlo por ejemplo [a-m]"

txt= "Hola mundo"
x=re.findall("[a-m]",txt)
print(x)

#. implica que puede existir cualquier caracter menos "newline"
#ho..la

txt="hello h##o heiio he wrld"
x= re.findall("he..o", txt)
print(x)

#\ secuencia especial "\d" 
txt= "Adrian cumplió 100 años"
x=re.findall("\d", txt)
print(x)

#^ Empieza con cierta cadena, letra, letras, etcétera, en específico
#[^5] empata todo menos el 5 hola5

txt="Hola mundo"
x=re.findall("Hola", txt)
if x:
    print("La cadena comienza con Hola")
else:
    print("La cadena no comienza con Hola")
    
#$ es para especificar con que patrón termina la cadena

txt= "Hola Mundo"
x=re.findall("Mundo$", txt)
if x:
    print("La cadena termina con Mundo")
else:
    print("la cadena no termina con Mundo")

#+ Una o más ocurrencias
strr= "Ana compró una manzana y una bolsa"
x=re.findall("an+", strr)
print(x)
if x:
    print("Hay al menos un match")
else:
    print("No hay ningún match")

#* 0 o más apariciones
strr= "Ana compró una manzana y una bolsa"
x=re.findall("an*", strr)
print(x)
if x:
    print("Hay al menos un match")
else:
    print("No hay ningún match")

#{} especificamos el número de ocurrencias de cierto patrón
#al{2}    

strs="a Daniel no le gusta la comida salada"
x= re.findall("al{1}",strs)
print(x)
if x:
    print("Hay un match")
else:
    print("no hay ningún match")
    
#| uno u otro "Hola|adiós"

txt= "Te gustan las peras o las manzanas?"
x= re.findall("peras|manzanas", txt)
print(x)
if x:
    print("Hay al menos un match")
else:
    print("No hay ningún match")
