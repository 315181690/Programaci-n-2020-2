#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 13:31:38 2020

@author: alonso2499
"""
import random
m=27
n=5
def adivinaA(n,m):
    count=0
    a=random.randint(n,m)
    while True:
        count+=1
        k=int(input("Introduce un número\n"))
        if k==a:
            print("Correcto, el número de intentos fue:", count)
            break
        elif k!=a:
            print("Inténtalo nuevamente")
            
adivinaA(n,m)

def adivinaB(n,m):
    a=random.randint(n,m)
    count1=0
    count2=0
    while True:
        k=int(input("Introduce un número\n"))
        if k<n:
            count1+=1
        elif k>m:
            count2+=1
        if k==a:
            print("Correcto, adivinaste\nValores antes del intervalo %i\nValores después del intervalo %i"%(count1,count2))
            break
        elif k!=a:
            print("Inténtalo nuevamente")
adivinaB(n,m)