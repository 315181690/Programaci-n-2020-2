# -*- coding: utf-8 -*-
"""
Created on Wed Feb 26 23:35:42 2020

@author: Programación
"""

from sympy import symbols, solve
x, a, b, c, d, e = symbols("x a b c d e")

a = (10000+x)*1.041
b = (a+x)*1.041
c = (b+x)*1.041
d = (c+x)*1.041
e = (d+x)*1.041
f = 50000-e
print(solve(f))