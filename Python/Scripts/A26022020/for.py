#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 23 13:36:28 2020

@author: alonso2499
"""

from utilidades import espera, agregaRutas
agregaRutas(['/Programaci-n-2020-2/Python/', '/Programaci-n-2020-2/Scripts/26022020/'],"home")

cadena = "Esta es una cadena no muy larga"
for caracter in cadena:
    print(caracter)
espera()

materias=["Algebra II", "Cálculo II", "Programación", "Geometría II", "Ciencia Básica", "Inglés I"]
for materia in materias:
    print(materia)
espera()

tercer=["Algebra Lineal I", "Calculo III", "Inglés II", "Manejo de Datos", "Probabilidad", "Taller de Modelación"]
cuarto=["Algebra Lineal II", "Calculo IV", "Ecuaciones Diferenciales I", "Inglés III", "Investigación de Operaciones", "Probabilidad II"]
quinto= ["Análisis Matemático I", "Análisis Numérico", "Inferencia Estadística", "Taller de Modelación II", "Taller de Redacción", "Variable compleja I"]
sexto=["Análisis Matemático Aplicado", "Inglés IV", "Procesos Estocásticos", "Sistemas dinámicos no lineales", "Optativa", "Optativa Requerida"]
septimo=["Ecuaciones diferenciales parciales I", "Formación científica I", "Inglés V", "Proyecto I", "Optativa", "Optativa"]
octavo=["Formación científica II", "Inglés VI", "Proyecto II", "Optativa", "Optativa", "Optativa"]
carrera=[["Algebra I", "Cálculo I", "THC", "Geomtería I", "Matemáticas Discretas"], materias, tercer, cuarto, quinto, sexto, septimo, octavo]
for semestre in carrera:
    print(semestre)
espera()

edades=[17,18,19,20,21]
for edad in edades:
    print(edad)
espera()