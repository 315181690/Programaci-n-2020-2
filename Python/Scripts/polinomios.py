#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  8 13:45:30 2020

@author: alonso2499
"""
import time
"1+2x¹⁰⁰"
"pl[0]x⁰ +pl[100]x¹⁰⁰"
pl=[0]*101; pl[0]=1; pl[100]=2
pg=[0]*101; pg[0]=5;pg[2]=2; pg[100]=-3
pd={0:1,100:2}
pll=[[0,1],[100,2]]
plt=[(0,1),(100,2)]
"x=5"
"1+2(5)¹⁰⁰"
x=1
n=len(pl)
s=0 #suma
for i in range(n):
    s+=pl[i]*x**i
print(s)

def evalua_polinomio_lista(coeficientes,x):
    s=0 #suma
    n=len(coeficientes)
    for i in range(n):
        s+=coeficientes[i]*(x**i)
    return s
def evalua2_polinomio_lista(coeficientes,x):
    return sum([coeficientes[i]*x**i for i in range(len(coeficientes))])
print(evalua_polinomio_lista(pl,1))
print(evalua2_polinomio_lista(pl,1))

def evalua_polinomio_dic(polinomio, x):
	suma=0
	for potencia in polinomio:
		suma+=polinomio[potencia] * x**potencia
	return suma
print(evalua_polinomio_dic(pd,1))

#Suma de polinomios por listas
#Si la longitud del polinomio 1 es mayor a la del p2 entonces usar range(len(p1))
#Se puede extender esto a sumar más de dos polinomios como con el mcd(a,b,c*)
#El resultado en un tercer polinomio
def suma_polinomios_lista(polinomio1,polinomio2):
    plr=[0]*len(polinomio1)
    plr=[polinomio1[potencia]+polinomio2[potencia] for potencia in range(len(polinomio1))]
    return plr
print(suma_polinomios_lista(pl,pg))
pl3=[0]*101; pl3[0]=7;pl[100]=3
def suma_mas_polinomios_lista(polinomio1,polinomio2,*polinomio3):
    plr=suma_polinomios_lista(polinomio1, polinomio2)
    for polinomio in polinomio3:
        plr=suma_polinomios_lista(plr,polinomio)
    return plr
print(suma_mas_polinomios_lista(pl,pg,pl3))
#plr=[plr[potencia]+polinomio3[potencia] for potencia in range(len(polinomio3))]
t = [27,28,26,24,28]
tf = [(grado*(9/5)+32 for grado in t)]

ps=[[0,1], [2,1], [2,3], [2,-5],[0,25],[100,1]]
"""def simplificar_polinomio(polinomio):
    pr=polinomio.pop()
    n=len(polinomio)
    polres=[]
    piter=[]
    n=0
    for i in range(n):
        pit=polinomio.pop()
        if pr[0]==pit[0]:
            n=pr[1]+pit[1]
            piter=[pr[0],n]
            polres.append(piter)
            piter=[0]
        else:
            
    return presul

def sim_pol(polinomio):
    n=len(polinomio)
    for i in range(n):
        if lista
print(simplificar_polinomio(ps))"""
tiempo_inicial=time.time()
print(evalua_polinomio_lista(pl,1))
print("--- %s segundos" %(time.time() - tiempo_inicial))
tiempo_inicial=time.time()
print(evalua_polinomio_dic(pd,1))
print("--- %s segundos" %(time.time() - tiempo_inicial))