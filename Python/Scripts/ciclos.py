#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 24 13:04:11 2020

@author: alonso2499
"""
n=int(input("Introduzca aquí la variable\n"))

def suma(n):
    suma=0
    for n in range(1,n+1):
        suma += n
        n-=1
    print("La suma de enteros es:", suma)
suma(n)
def sumaParesW(n):
    suma=0
    while n>0:
        if (n%2)==0:
            suma+=n
        n-=1
    print("La suma de los números pares while es:", suma)
#llamada a la funcion
sumaParesW(n)

def sumaPares(n):
    suma=0
    for n in range(1,n+1):
        if (n%2)==0:
            suma +=n
        n-=1
    print("La suma de los números pares es:", suma)
sumaPares(n)

def sumaImparesW(n):
    suma=0
    while n>0:
        if (n%2)==1:
            suma+=n
        n-=1
    print("La suma de los números impares while es:", suma)
sumaImparesW(n)

def sumaImpares(n):
    suma=0
    for n in range(1,n+1):
        if (n%2)==1:
            suma +=n
        n-=1
    print("La suma de los números impares es:", suma)
sumaImpares(n)

def numeracion(n):
    for i in range(n):
        print(i+1)
numeracion(n)

def numeracionDecreciente(n):
    for i in range(1,n+1):
        n-=1
        print(n+1)
numeracionDecreciente(n)
