#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 21 13:11:46 2020

@author: alonso2499
"""

#Escribe un programa en Python que acepte una cadena y calcule el número de
#Digitos y letras

def digitos_letras():
    cadena=input("Ingrese una cadena:\n")
    d=[]
    l=[]
    for caracter in cadena:
        if caracter.isdigit()==True:
            d.append(caracter)
        if caracter.isalpha()==True:
            l.append(caracter)
    td=len(d)
    tl=len(l)
    return ("El número de dígitos es: %g y el número de letras es: %g"%(td,tl))
#Escribe un programa de Python para calcular la edad de un perro en los años
#del perro
#Primero versión general tomar cualquiera de las 3 listas
def años_perro():
    edad=int(input("Introduzca la edad de su perro:\n"))
    pequeño={1:15,2:24,3:28,4:32,5:36,6:40,7:44,8:48,9:52,10:56,11:60,12:64}
    for e in pequeño:
        if edad==e:
            ep=pequeño[e]
    return ("La edad de tu perro en años humano es: %g"%(ep))
#Cuanto pesa en kilos el perro y la edad para calcular su edad en años de perro
def años_perro_gen():
    peso=int(input("Introduzca el peso (en kg) de su perro:\n"))
    edad=int(input("Introduzca la edad de su perro:\n"))
    pequeño={1:15,2:24,3:28,4:32,5:36,6:40,7:44,8:48,9:52,10:56,11:60,12:64}
    mediano={1:15,2:24,3:28,4:32,5:36,6:42,7:47,8:50,9:51,10:60,11:65,12:69}
    grande={1:15,2:24,3:28,4:32,5:36,6:45,7:50,8:55,9:61,10:66,11:72,12:77}
    if peso<=9:
        for e in pequeño:
            if edad==e:
                ep=pequeño[e]
    if 9<peso<23:
        for e in mediano:
            if edad==e:
                ep=mediano[e]
    if peso>23:
        for e in grande:
            if edad==e:
                ep=grande[e]
    return ("La edad de tu perro en años humano es: %g"%(ep))