#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 11:08:43 2020

@author: alonso2499
"""

#Para tributar un determinado impuesto se debe ser mayor de 16 años y tener unos ingresos superiores a 1000 mensuales. Escribir un programa que pregunte al usuario su edad y sus ingresos mensuales y muestre por pantalla si el usuario tiene que tributar o no.
def tributar():
    edad=int(input("Ingresa tu edad\n"))
    salario=int(input("Ingresa tu salario mensual\n"))
    if edad >=16 and salario >=1000:
        return "Puedes tributar"
    else:
        return "No puedes tributar"
    
if __name__ == "__main__":
    print(tributar())
else:
    pass