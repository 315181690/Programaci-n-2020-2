#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 28 13:05:15 2020

@author: alonso2499
"""
cadena1="Pedro"
cadena2="Poder"
vc=["á","é","í","ó", "ú"]
vs=["a","e","i","o", "u"]
def anagrama(cadena1,cadena2):
    c1=cadena1.lower()
    c2=cadena2.lower()
    c1=cadena1.replace("á","a")
    c1=cadena1.replace("é","e")
    c1=cadena1.replace("í","i")
    c1=cadena1.replace("ó","o")
    c1=cadena1.replace("ú","u")
    c2=cadena2.replace("á","a")
    c2=cadena2.replace("é","e")
    c2=cadena2.replace("í","i")
    c2=cadena2.replace("ó","o")
    c2=cadena2.replace("ú","u")
    l1=sorted(c1)
    l2=sorted(c2)
    co=0
    for i in range(len(c1)):
        if l1[i]==l2[i]:
            co+=0
        else:
            co+=1
    if co==0:
        print(" %s es un anagrama de %s"%(cadena2,cadena1))
    else:
         print("No es un anagrama")
         
def esAnagrama(c1,c2):
    c1=c1.lower()
    c2=c2.lower()
    c1=c1.replace("á","a")
    c1=c1.replace("é","e")
    c1=c1.replace("í","i")
    c1=c1.replace("ó","o")
    c1=c1.replace("ú","u")
    c2=c2.replace("á","a")
    c2=c2.replace("é","e")
    c2=c2.replace("í","i")
    c2=c2.replace("ó","o")
    c2=c2.replace("ú","u")
    cont={}
    for char in c1:
        if char not in cont:
            cont[char]=0
        cont[char]+=1
    print(cont)
    for char in c2:
        if char not in cont:
            cont[char]=0
        cont[char]-=1
    print(cont)
    for key in cont:
        if cont[key]!=0:
            return "No es anagrama"
    return "Es anagrama"

print(anagrama(cadena1,cadena2))
print(esAnagrama(cadena1,cadena2))
print(esAnagrama("lucía", "licúa"))