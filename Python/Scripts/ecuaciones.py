
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 15 13:09:29 2020

@author: David Alonso Garduño Granados
"""
def valorY(a1,a2,b2,b1,c,f):
    """
Usando el método de sustitución de u na ecuacion de dos ecuaciones por dos incógnitas de la forma:
a1x+b1y=c
a2x+b2y=f"""
    if b1-b2!=0:
        ctey=c/b1#El valor de las constantes en la primera ecuacion, lo usé así para no tener que cargar con esta operación todo el tiempo
        dx=a2-((b2*a1)/b1)#De la segunda ecuación realizo la suma asociada a los coeficientes de x
        x=(f-b2*ctey)/(dx)#El valor de X despejado de la segunda con la sustitución de Y
        y=(c-a1*(x))/b1 #El despeje original de la primera ecuación
        print("El valor de X es %g y el valor de Y es %g"%(x,y))
        
    else:
        print("La ecuación no tiene solucion")

a1=float(input("\nIntroduzca el coeficiente de x en la primera ecuación\n"))
b1=float(input("\nIntroduzca el coeficiente de y en la primera ecuación\n"))
c=float(input("\nIntroduzca el valor de la constante en la primera ecuación\n"))
a2=float(input("\nIntroduzca el coeficiente de x en la segunda ecuación\n"))
b2=float(input("\nIntroduzca el coeficiente de y en la segunda ecuación\n"))
f=float(input("Introduzca el valor de la constante en la segunda ecuación\n"))
valorY(a1,a2,b2,b1,c,f)

#Acá igualación por mí
def igualacion(a1,b1,c,a2,b2,f):
    g=float(input("\n Introduzca valor de g\n"))
    if a2-(a1*g)==0:
        y=(f-(c*g))/(b2-(b1*g))
        x=c-(b1*y)
    if b2-(b1*g)==0:
        x=(f-(c*g))/(a2-(a1*g))
        y=c-(a1*x)
    print(x,y)
def cramer(a, b, c, d, e, f):
    """Se quiere dar solución al sistema de ecuaciones
    ax + by = c
    dx + ey = f
    donde
    a -> es el coeficiente de la x de la primera ecuación
    b -> es el coeficiente de la y de la primera ecuación
    c -> es el coeficiente independiente de la primera ecuación
    d -> es el coeficiente de la x de la segunda ecuación
    e -> es el coeficiente de la y de la segunda ecuación
    f -> es el coeficiente independiente de la segunda ecuación
    FUNCIÓN HECHA POR: LAGUNA BARRIOS VALERIA
    """
    u = a*e - b*d
    v = c*e - b*f
    w = a*f - c*d
    """
    u el el determinante de la matriz |a b|
                                      |d e|
    v es el determinante de la matriz |c b|
                                      |f e|
    w es el determinante de la matriz |a c|
                                      |d f|
    """
    x = v / u #x es la incognita que buscamos como primera entrada
    y = w / u #y es la incognita que buscamos como segunda entrada
    return (x,y)
def reduccion(a,b,c,d,e,f):
    x=(c-f)/(b-e)
    y=(c-b*x)/a    
    if a==d and b!=e:
        r1=x
        r2=y
        print('En tu sistema lineal, y es igual a',r1,'x es igual a',r2)
    elif a==d and b==e:
        print('El sistema lineal tiene renglones linealmente dependientes, por lo que no se puede resolver')

    elif a!=0 and d!=0:
        a_0=a
        d_0=d
        a=a*d_0
        b=b*d_0
        c=c*d_0
        d=d*a_0
        e=e*a_0
        f=f*a_0
        r1,r2=despeje(a,b,c,d,e,f)
        print('En tu sistema lineal, y es igual a',r1,',x es igual a',r2)
    else:
        print('Elige otro sistema de ecuaciones')

def despeje(a,b,c,d,e,f):
  x=(c-f)/(b-e)
  y=(c-b*x)/a
  return x,y


         

if a==d and b!=e:
  r1,r2=despeje(a,b,c,d,e,f)
  print('En tu sistema lineal, x es igual a',r1,'y es igual a',r2)

elif a==d and b==e:
	print('El sistema lineal tiene renglones linealmente dependientes, por lo que no se puede resolver')

elif a!=0 and d!=0:
  a_0=a
  d_0=d
  a=a*d_0
  b=b*d_0
  c=c*d_0
  d=d*a_0
  e=e*a_0
  f=f*a_0
  r1,r2=despeje(a,b,c,d,e,f)
  print('En tu sistema lineal, x es igual a',r1,'y es igual a',r2)
else:
  print('Elige otro sistema de ecuaciones')
