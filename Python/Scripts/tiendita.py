# -*- coding: utf-8 -*-


inventario = {
    1:{"Nombre":"Coca",
            "Precio":15.00,
            "Cantidad":1000},
    2:{"Nombre":"Sabritas Amarillas",
            "Precio":12.00,
            "Cantidad":600},
    3:{"Nombre":"Galletas Marias",
            "Precio":10.00,
            "Cantidad":300}
    }

cajaChica = 1000

formato_venta_compra = { 
    "productos" : {
        1:5,#llave es el codigo, el valor la cantidad
        2:10,
        3:4
        },
    "total":235, #(5*15)+(10*12)+(4*10) #total de la compra
    "tipo":"Compra"
    }

#Con este ejercicio vamos a simular el funcionamiento de una tiendita la cual
#Tiene una caja_chica que es el dinero con el que cuenta
#y un diccionario con Productos (inventario) que es los productos que la tiendita tiene 
#para vender


#Para simular las trasaccion de compra venta de productos vamos
#A utilizar un diccionario como ejemplo formato_venta_compra

#El primer ejecico sera hacer un metodo que muestre el inventario en la pantalla
#con un formato que se vea bien

#El segundo ejercicio a realizar es un metodo que dado que si le pasamos un 
#diccionario con el formato de venta-compra modifique la caja_chica y la
#cantida de productos dentro del inventario

#Imprime con formato "bonito" el inventario
def imprimeInventarioCaja():
    print ("Tenemos los siguientes productos disponibles:")
    for key in inventario:
        nombre = inventario[key]["Nombre"]
        precio = inventario[key]["Precio"]
        cantidad = inventario[key]["Cantidad"]
        print ("\t"+nombre+"  \n\t\tPrecio:"+str(precio)+" Disponibles:"+str(cantidad))
    print ("La caja chica tiene: "+str(cajaChica))

def transaccion(transaccion):
    global cajaChica#si se quita esta linea marca errores porque no puede modificarse, se tiene que poner global para poderlo modificar
    tipo = transaccion["tipo"]
    productos = transaccion["productos"]
    total = transaccion["total"]
    
    if tipo=="Compra":
        for key in productos:
            num_productos = productos[key]
            inventario[key]["Cantidad"] += num_productos
            
        cajaChica -= total
        
    if tipo=="Venta":
        for key in productos:
            num_productos = productos[key]
            inventario[key]["Cantidad"] -= num_productos
            
        cajaChica += total

imprimeInventarioCaja()
compra = { #el tipo es compra o venta
    "productos" : {
        1:50,
        2:50,
        3:50
        },
    "total":950, 
    "tipo":"Compra"
    }
transaccion(compra)
print("El inventario despues de la compra")
imprimeInventarioCaja()

venta = { 
    "productos" : {
        1:5,
        2:5,
        3:5
        },
    "total":95, 
    "tipo":"Venta"
    }
transaccion(venta)
print("El inventario despues de la venta")
imprimeInventarioCaja()
