#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 14 13:22:18 2020

@author: alonso2499
"""

def pangrama(cadena):
    cadena=cadena.lower()
    abc="abcdefghijklmnopqrstuvwxyzñ"
    c=cadena.replace("á","a")
    c=cadena.replace("é","e")
    c=cadena.replace("í","i")
    c=cadena.replace("ó","o")
    c=cadena.replace("ú","u")
    c=cadena.replace(" ","")
    c=cadena.replace(",","")
    c=cadena.replace(".","")
    c=cadena.replace("¿","")
    c=cadena.replace("?","")
    c=cadena.replace("!","")
    c=cadena.replace("¡","")
    co=0
    for caracter in abc:
        if caracter not in c:
            co+=0
        else:
            co+=1
    if co==27:
        print("Es un pangrama")
    else:
        print("No es un pangrama")
pangrama("Benjamín pidió una bebida de kiwi y fresa. Noé, sin vergüenza, la más exquisita champaña del menú")
pangrama("pangrama")

def trianguloPascal(n):
    l1=[[1],[1,1]]
    for i in range(1,n):
        lp=[1]
        for j in range(0,len(l1[i])-1):
            lp.extend([l1[i][j]+l1[i][j+1]])
        lp+=[1]
        l1.append(lp)
    return l1

print(trianguloPascal(10))