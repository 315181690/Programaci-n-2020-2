#!/usr/bin/env python3
# -*- coding: utf-8 -*-
def limpiaPantalla():
    """"
    Limpia la pantalla de la terminar
    o de una ventana de línea de comandos
    de windows.
    """
    import platform, os
    if platform.system() == 'Linux':
        os.system('clear')
    elif platform.system() == 'Windows':
        os.system('cls')
    elif platform.system() == 'Darwin':
        os.system('clear')