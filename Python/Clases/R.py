#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 24 13:17:30 2020

@author: alonso2499
"""

class R:
    #Miembros
    x=0
    na=0 #Neutro aditivo
    nm=1 #Neutro multiplicativo
    def __init__(self, valor=-1):
        self.x=valor
        #Métodos
        #self el objeto desde el cual estoy mandando a llamar al método
if __name__ == "__main__":
    r1=R()
    print(r1.x)